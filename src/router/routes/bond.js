// import requiredAuth from './guards/required-auth';
import List from '@/views/bond/List.vue';

export default [
  {
    path: '/bonds',
    name: 'bonds.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];