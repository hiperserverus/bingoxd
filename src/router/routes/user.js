import requiredAuth from './guards/required-auth';
import List from '@/views/user/List.vue';
import Show from '@/views/user/Show.vue';
import Edit from '@/views/user/Edit.vue';
import Create from '@/views/user/Create.vue';

export default [
  {
    path: '/user/list',
    name: 'user.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
  {
    path: '/user/create',
    name: 'user.create',
    component: Create,
    // beforeEnter: requiredAuth,
  },
  {
    path: '/user/show/:id',
    name: 'user.show',
    component: Show,
    beforeEnter: requiredAuth,
  },
  {
    path: '/user/edit/:id',
    name: 'user.edit',
    component: Edit,
    // beforeEnter: requiredAuth,
  },
];
