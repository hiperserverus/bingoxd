// import requiredAuth from './guards/required-auth';
import List from '@/views/report/List.vue';

export default [
  {
    path: '/reports',
    name: 'reports.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];
