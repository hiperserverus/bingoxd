import List from '@/views/administrator/List.vue';

export default [
  {
    path: '/administrators/list',
    name: 'administrator.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];