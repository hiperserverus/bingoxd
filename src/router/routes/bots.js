// import requiredAuth from './guards/required-auth';
import List from '@/views/bots/List.vue';

export default [
  {
    path: '/bots/:id?',
    name: 'bots.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];