// import requiredAuth from './guards/required-auth';
import List from '@/views/group/List.vue';

export default [
  {
    path: '/groups/list',
    name: 'group.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];