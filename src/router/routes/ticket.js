// import requiredAuth from './guards/required-auth';
import List from '@/views/ticket/List.vue';

export default [
  {
    path: '/tickets',
    name: 'tickets.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];