import auth from './auth';
import user from './user';
import profile from './profile';
import report from './report';
import ticket from './ticket';
import terminal from './terminal';
import bots from './bots';
import statistic from './statistic';
import country from './country';
import bond from './bond';
import administrator from './administrator';
import group from './group';
import branch from './branch';

export default [
  ...auth,
  ...user,
  ...profile,
  ...report,
  ...ticket,
  ...terminal,
  ...bots,
  ...statistic,
  ...country,
  ...bond,
  ...administrator,
  ...group,
  ...branch

];
