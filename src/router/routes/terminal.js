// import requiredAuth from './guards/required-auth';
import List from '@/views/terminal/List.vue';

export default [
  {
    path: '/terminals',
    name: 'terminals.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];