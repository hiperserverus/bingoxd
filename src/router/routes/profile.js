// import requiredAuth from './guards/required-auth';
import Edit from '@/views/profile/Edit.vue';

export default [
  {
    path: '/profile/:id?',
    name: 'profile.edit',
    component: Edit,
    // beforeEnter: requiredAuth,
  },
];
