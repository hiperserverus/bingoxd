// import requiredAuth from './guards/required-auth';
import List from '@/views/statistic/List.vue';

export default [
  {
    path: '/statistics',
    name: 'statistic.list',
    component: List,
    // beforeEnter: requiredAuth,
  },
];
