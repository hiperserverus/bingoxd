import Vue from 'vue';
import axios from 'axios';
import App from '@/views/App.vue';
import vuetify from '@/config/vuetify';
import router from '@/router';
import store from '@/store';
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/dist/vuetify.min.css';

import Dashboard from '@/layouts/dashboard.vue';
import NoDashboard from '@/layouts/no-dashboard.vue';

require('./bootstrap');

// Layouts
Vue.component('dashboard', Dashboard);

Vue.component('no-dashboard', NoDashboard);

// axios
const baseUrl = 'http://192.168.1.2:9072/bingoretailapi';
axios.defaults.baseURL = baseUrl;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.headers.common['Accept'] = 'application/json, text/plain, */*';
axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin';

const crsfToken = document.head.querySelector('meta[name="csrf-token"]');
if (crsfToken) {
  axios.defaults.headers.common['X-CSRF-TOKEN'] = crsfToken.content;
}

const accessToken = localStorage.getItem('access_token');
if (accessToken) {
  axios.defaults.headers.common.Authorization = accessToken;
}

// $http
Vue.prototype.$http = axios;

// router
Vue.router = router;

// store
Vue.store = store;

App.router = Vue.router;
App.store = Vue.store;

new Vue({
  vuetify,
  render: h => h(App),
}).$mount('#app');
